# Implementación del Juego de la Vida con Dinámicas de Dos Especies

Este proyecto presenta una variación del clásico "Juego de la Vida" de Conway, en el que se introduce la dinámica de dos especies celulares. Además, se permite la modificación de una regla del juego para explorar diferentes comportamientos emergentes.

## Descripción del Proyecto

### Implementación del Juego de la Vida con Dos Especies
- Se implementa el juego de la vida con dos especies celulares siguiendo las condiciones discutidas en clase. Las reglas específicas se detallan en el código.

### Modificación de una Regla del Juego
-Se elige la siguiente regla del juego para modificar, brindando la posibilidad de:
  - Ajustar los criterios para el nacimiento de una célula muerta como célula tipo A o B (estos criterios se especifican en el código).

### Obtención de Distribuciones de Células A y B
- Se analizan las distribuciones del número de células A y B a lo largo de 1000 iteraciones del juego. Se emplean herramientas de visualización como `matplotlib.pyplot.hist` y `matplotlib.pyplot.hist2d`.

### Consideraciones sobre el Sistema
- Se considera el sistema en términos de los estados NA y NB, donde NA representa el número de células tipo A y NB el número de células tipo B.

### Algoritmo de Metrópolis
- Se utiliza el algoritmo de Metrópolis para generar una serie de estados del sistema, basándose en las distribuciones obtenidas en el punto 3.

### Gráfico de Probabilidad 2-Dimensional
- Se grafica la probabilidad 2-dimensional de los estados generados con el algoritmo de Metrópolis. Se compara cualitativamente con las distribuciones obtenidas en el punto de análisis de distribuciones.

### Contribuidores
Este proyecto fue desarrollado para el seguimiento 2 del curso de física computacional I del Instituto de Física, Universidad de Antioquia, por el equipo conformado por:

Caterine Bedoya (@caterine.bedoyab)
Jhonatan Jurado (@JhonatanJ)